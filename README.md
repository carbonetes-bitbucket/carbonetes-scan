# Bitbucket Pipelines Pipe: Carbonetes

This pipe seamlessly integrates comprehensive container analysis directly into your CI/CD pipeline.

## YAML definition

Add the following **snippet** to the **script** section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: carbonetes/carbonetes-scan:1.0.0
  variables:
    CARBONETES_USERNAME: "<string>"
    CARBONETES_PASSWORD: "<string>"
    REGISTRY_URI: "<string>"
    REPO_IMAGE_TAG: "<string>"
    # FAIL_ON_POLICY: "<boolean>" # has a default value of false. Changes value depending on the result of policy evaluation.
```

## Variables

| Variables                   | Usage                                                                                        |
| --------------------------- | -------------------------------------------------------------------------------------------- |
| CARBONETES_USERNAME \*      | The account username on Carbonetes. |
| CARBONETES_PASSWORD \*      | The account password on Carbonetes. |
| REGISTRY_URI \*             | The registry uri is managed in Carbonetes. |
| REPO_IMAGE_TAG \*           | The image to be scan. |
| FAIL_ON_POLICY              | Decides if it build or stop the build based on the policy evaluation. Default: `false`. |

_\* = required variable._

*Note : to be aligned in your CI/CD pipeline, make sure that you supply the same REPO_IMAGE_TAG that has been built within your pipeline stages. See below sample pipeline stages.* 

![Pipeline Stages](./assets/sample-pipeline-stages.png)

## Details

By merging Carbonetes into Bitbucket Pipelines, this pipe can seamlessly analyze your code for security risks, check the results against the policy and automatically build or stop the build based on that policy evaluation.

Upon running the pipeline, the pipe automatically initiates a comprehensive container analysis scan. The results of that scan are compared to the applicable policy to determine whether the container should build or not. The insight from the analysis and the policy evaluation are embedded right inside this pipe making it easy to find and resolve issues without ever leaving Bitbucket.

To discover more about Carbonetes, check [Carbonetes website](https://carbonetes.com/).

## Prerequisites

This pipe requires a valid **Carbonetes credentials** `(email and password)`.

- Doesn't have any credentials yet? [Register now](https://console.carbonetes.com/register).
- Add all the credentials inside the `repository variables` as [secured environment variables](https://support.atlassian.com/bitbucket-cloud/docs/variables-and-secrets/#Environmentvariables-Securedvariables).

## Example

### Docker image scan example
Uses Carbonetes to seamlessly integrate comprehensive container analysis directly to analyze a Docker image.

```yaml
script:
    - IMAGE_NAME=${REPO_IMAGE_TAG}
    - docker build -t $IMAGE_NAME -f Dockerfile .
    
    - pipe: carbonetes/carbonetes-scan:1.0.0
      variables:
        CARBONETES_USERNAME: ${CARBONETES_USERNAME}
        CARBONETES_PASSWORD: ${CARBONETES_PASSWORD}
        REGISTRY_URI: ${REGISTRY_URI}
        REPO_IMAGE_TAG: ${IMAGE_NAME}
        # Note: to be aligned in your CI/CD pipeline,
        #       make sure that you supply the same REPO_IMAGE_TAG
        #       that has been built within your pipeline stages.

    - docker push $IMAGE_NAME
```

## Support
To help with this pipe, or have an issue or feature request, please contact: [eng@carbonetes.com](eng@carbonetes.com)

If reporting an issue, please include:

* the version of the pipe
* relevant logs and error messages
* steps to reproduce

## License and Copyright

Copyright © 2020 Carbonetes

Licensed under [MIT License](./LICENSE.txt).