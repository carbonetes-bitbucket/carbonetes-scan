FROM alpine:3.9

RUN wget -P / https://bitbucket.org/bitbucketpipelines/bitbucket-pipes-toolkit-bash/raw/0.4.0/common.sh

RUN apk add --no-cache --upgrade bash

COPY packages /  
RUN chmod a+x /packages.sh
RUN /packages.sh

COPY pipe /
RUN chmod a+x /*.sh

ENTRYPOINT ["/pipe.sh"]