## Releasing a new version

This pipe uses an automated release process to bump versions using semantic versioning and generate the CHANGELOG.md file automatically. 

In order to automate this process it uses a tool called [`semversioner`](https://pypi.org/project/semversioner/). 

### Steps to release

1) Install semversioner locally.

```sh
pip install semversioner
```

2) During development, every changes that were need to be integrated to `master` will need one or more changeset files. Use semversioner to generate changeset. Use either of: `--type major` `--type minor` or `--type patch` depends on what type of version will be released.

```sh
semversioner add-change --type <patch|minor|major> --description "Fix security vulnerability with authentication."
```

3) Commit the changeset files generated in `.semversioner/next-release/` folder with your code. Example:

```sh
git add .
git commit -m "BP-234 FIX security issue with authentication"
git push origin 
```

4) Merge `release` branch into `master` and Bitbucket Pipelines will do the rest:

* Generate new version number based on the changeset types `major`, `minor`, `patch`.
* Generate a new file in `.semversioner` directory with all the changes for this specific version.
* (Re)generate the CHANGELOG.md file.
* Bump the version number in `README.md` example and `pipe.yml` metadata.
* Commit and push back to the repository.
* Tag your commit with the new version number.

5). Create a `pull request` to `master` and merge it back to `develop`.