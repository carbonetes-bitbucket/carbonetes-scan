#!/usr/bin/env bats

setup() {
  PIPE_IMAGE=${DOCKER_IMAGE:="test/carbonetes-scan"}
  run docker build -t ${PIPE_IMAGE} .
}

teardown() {
    echo "Teardown happens after each test."
}

@test "Tests" {
    run docker container run \
        --env=REGISTRY_URI="$REGISTRY_URI" \
        --env=REPO_IMAGE_TAG="$REPO_IMAGE_TAG" \
        --env=CARBONETES_USERNAME="$CARBONETES_USERNAME" \
        --env=CARBONETES_PASSWORD="$CARBONETES_PASSWORD" \
        ${PIPE_IMAGE}

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 0 ]
}
