#!/bin/sh

apk add --update --no-cache curl
apk add --update --no-cache jq
apk add --update --no-cache coreutils
apk add --update --no-cache gawk

echo "Done installing required packages.."